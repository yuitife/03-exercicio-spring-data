package com.itau.funcionarios.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.itau.funcionarios.models.Departamento;
import com.itau.funcionarios.repositories.DepartamentoRepository;

@Service
public class DepartamentoService {
	@Autowired
	DepartamentoRepository departamentoRepository;
	
	public Iterable<Departamento> buscarTodos() {
		return departamentoRepository.findAll();
	}
}
