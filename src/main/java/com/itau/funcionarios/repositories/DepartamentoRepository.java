package com.itau.funcionarios.repositories;

import org.springframework.data.repository.CrudRepository;
import com.itau.funcionarios.models.Departamento;

public interface DepartamentoRepository extends CrudRepository<Departamento, Integer>{

}
